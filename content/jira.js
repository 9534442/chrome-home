console.log('scanning');

function handleTextNode(textNode) {
	if(textNode.nodeName !== '#text'
		|| textNode.parentNode.nodeName === 'SCRIPT'
		|| textNode.parentNode.nodeName === 'A'
	) {
		//Don't do anything except on text nodes, which are not children
		//  of <script> or <style>.
		return;
	}
	let origText = textNode.textContent;
	let newHtml=origText.replace(/((?!([A-Z0-9a-z]{1,10})-?$)[A-Z]{1}[A-Z0-9]+-\d+)/g,(text) => {
		return `<a href="https://dev-factoryfour.atlassian.net/browse/${text}">${text}</a>`;
	});
	//Only change the DOM if we actually made a replacement in the text.
	//Compare the strings, as it should be faster than a second RegExp operation and
	//  lets us use the RegExp in only one place for maintainability.
	if( newHtml !== origText) {
		let newSpan = document.createElement('span');
		newSpan.innerHTML = newHtml;
		textNode.parentNode.replaceChild(newSpan,textNode);
	}
}

function processDocument(node) {
	//Create the TreeWalker
	let treeWalker = document.createTreeWalker(node, NodeFilter.SHOW_TEXT,{
		acceptNode: function(node) {
			if(node.textContent.length === 0) {
				//Alternately, could filter out the <script> and <style> text nodes here.
				return NodeFilter.FILTER_SKIP; //Skip empty text nodes
			} //else
			return NodeFilter.FILTER_ACCEPT;
		}
	}, false );
	//Make a list of the text nodes prior to modifying the DOM. Once the DOM is
	//  modified the TreeWalker will become invalid (i.e. the TreeWalker will stop
	//  traversing the DOM after the first modification).
	let nodeList=[];
	while(treeWalker.nextNode()){
		nodeList.push(treeWalker.currentNode);
	}
	//Iterate over all text nodes, calling handleTextNode on each node in the list.
	nodeList.forEach(function(el){
		// console.log(el);
		handleTextNode(el);
	});
}
window.onload = function() {
	processDocument(document.body);
}

document.addEventListener("DOMNodeInserted", function(e) {
	processDocument(e.target);
}, false);
